package com.example.tetaanalyticscompose

import android.app.Application
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        // Creating an extended library configuration.
        val config =
            YandexMetricaConfig.newConfigBuilder("YA API").build()
        // Initializing the AppMetrica SDK.
        YandexMetrica.activate(applicationContext, config)
        // Automatic tracking of user activity.
        YandexMetrica.enableActivityAutoTracking(this)

    }
}