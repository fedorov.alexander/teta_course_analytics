package com.example.tetaanalyticscompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.tetaanalyticscompose.ui.theme.TetaAnalyticsComposeTheme

class MainActivity : ComponentActivity() {
    private val analyticsManager: AnalyticsManager by lazy { AnalyticsManagerImpl() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TetaAnalyticsComposeTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background) {
                    Greeting(
                        analyticsManager = analyticsManager,
                    )
                }
            }
        }
    }
}

@Composable
fun Greeting(
    analyticsManager: AnalyticsManager,
) {
    var counter by remember {
        mutableStateOf(1)
    }

    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        //Login – проставляет UserId ко всем последующим событиям
        Button(onClick = { analyticsManager.login("1234") }) {
            Text(text = "Login")
        }
        //Logout – удаляет UserId из всех последующих событий
        Button(onClick = { analyticsManager.logout() }) {
            Text(text = "Logout")
        }
        //Send Event – посылает ивент о действии пользователя
        Button(onClick = { analyticsManager.sendEvent("test event ${counter++}") }) {
            Text(text = "Send Event")
        }
        //Simulate Crash – имитирует падение приложения и отправляет лог в аналитику
        Button(onClick = { error("Simulate Crash") }) {
            Text(text = "Simulate Crash")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    TetaAnalyticsComposeTheme {
        Greeting(
            analyticsManager = AnalyticsManagerImpl(),
        )
    }
}
