package com.example.tetaanalyticscompose

import android.util.Log
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.yandex.metrica.YandexMetrica

interface AnalyticsManager {
    fun login(userId: String)
    fun logout()
    fun sendEvent(event: String)
}

class AnalyticsManagerImpl : AnalyticsManager {
    val firebaseAnalytics by lazy { Firebase.analytics }
    override fun login(userId: String) {
        Log.d(ANALYTICS_LOGGER_TAG, "login $userId")
        YandexMetrica.setUserProfileID(userId)
        firebaseAnalytics.setUserId(userId)
    }

    override fun logout() {
        Log.d(ANALYTICS_LOGGER_TAG, "logout")
        YandexMetrica.setUserProfileID(null)
        firebaseAnalytics.setUserId(null)
    }

    override fun sendEvent(event: String) {
        Log.d(ANALYTICS_LOGGER_TAG, "sendEvent $event")
        YandexMetrica.reportEvent(event)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Param.NUMBER_OF_PASSENGERS) {
            param(FirebaseAnalytics.Param.LEVEL_NAME, event)
        }
    }

    companion object {
        const val ANALYTICS_LOGGER_TAG = "AnalyticsLoggerTag"
    }
}